# Twitter Analyzer

Helps you find positive tweets about your brand!

## Installation

`npm install`

## Run in browser

`grunt serve`  
  
Visit in browser @ http://localhost:9000/

## Running Tests

`grunt test`

## What's next:  
- Unit tests
- Remove global $scope usage, should refactor to individual directives with isolate scope
	- Login/User directive
	- Search directive
- Break service.js in to services for each API
	- Twitter
	- Indico
- Code review `sentimentSearch` to ensure deferred promise chain implemented appropriately	
- Better ordering capability (rewrite `findTopIndexes()`)
- URI encoding for twitter search terms
- Fix bug where user is not aware they're authentication is no longer valid
- Fix bug where results from Twitter with low count somehow only rendering 2 'bestTweets'
- Fields and attrs to allow user to enter # of positive tweets to view, or to select different kinds of analysis
	- Political
	- Emotional
	- Relevance
- Add instructions for search term refinement
- Improve display of on-screen tweets
- Extract logging to service, remove excess



