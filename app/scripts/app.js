angular.module('twitterApp', [
	'ngRoute',
	'ngSanitize',
	'twitterApp.services'
	])
	.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'TwitterController',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
