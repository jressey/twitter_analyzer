//much credit to https://www.sitepoint.com/building-twitter-app-using-angularjs/

angular.module('twitterApp.services', []).factory('twitterService', function($q, $http) {

	var authorizationResult = false;
	var scores = [];

	return {
		initialize: function() {
            //initialize OAuth.io with public key of the application
            OAuth.initialize('4N7v95C6-q-8Pj--W0JySA_UqlM', {
            	cache: true
            });
            //try to create an authorization result when the page loads,
            // this means a returning user won't have to click the twitter button again
            authorizationResult = OAuth.create("twitter");
        },
        isReady: function() {
        	return (authorizationResult);
        },
        connectTwitter: function() {
        	var deferred = $q.defer();
        	OAuth.popup("twitter", {
        		cache: true
        	}, function(error, result) {
                // cache means to execute the callback if the tokens are already present
                if (!error) {
                	authorizationResult = result;
                	deferred.resolve();
                } else {
                    //do something if there's an error
                }
            });
        	return deferred.promise;
        },
        clearCache: function() {
        	OAuth.clearCache('twitter');
        	authorizationResult = false;
        },
        
        getLatestTweets: function(term) {
            //create a deferred object using Angular's $q service
            var deferred = $q.defer();
            var url = '/1.1/search/tweets.json';
            if (term) {
            	url += '?q=' + term;
            }
            //need to encode the url
            var promise = authorizationResult.get(url)
            .done(function(data) {

                // when the data is retrieved resolve the deferred object
                deferred.resolve(data);
            }).fail(function(err) {
            	console.log(err);
            	deferred.reject(err);
            });
            //return the promise of the deferred object
            return deferred.promise;
        }, 

        analyzeWithIndico: function(tweets) {
        	var deferred = $q.defer();
        	console.log('printing results of analyzer call');
        	var promise = $.ajax({
            type: "POST",
            url: 'https://apiv2.indico.io/sentiment/batch?key=a8ef5ca8d269432de86d598026c8d359',
            data: JSON.stringify({ 'data': tweets })
        	}).done(function(data) {
        		console.log("service data: " + data);
        		
                // when the data is retrieved resolve the deferred object
                deferred.resolve(data);
            }).fail(function(err) {
            	console.log(err);
            	deferred.reject(err);
            });
            //return the promise of the deferred object
            return deferred.promise;

        }
    }
});