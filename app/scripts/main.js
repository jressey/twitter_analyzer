//much credit to https://www.sitepoint.com/building-twitter-app-using-angularjs/

angular.module('twitterApp')
.controller('TwitterController', function($scope, $q, twitterService) {
    $scope.tweets = []; //array of tweets
    $scope.bestTweets = []; //array of tweets

    twitterService.initialize();

    $scope.sentimentSearch = function(term) {
         // console.log(term);
         twitterService.getLatestTweets(term)
         .then(function(data) {
            var tweets = data['statuses'];
            $scope.tweets = tweets;
            return tweets;
        }).then(function(tweets) {
            console.log(tweets);
            var scores = $scope.getSentimentScores(tweets);
            // console.log('scores: ' + scores);
            return scores;
        }).then(function(scores) {
            $scope.getBestTweets(scores);
        }, function() {
            $scope.twitterError = true;
        });
     }

     $scope.getSentimentScores = function(tweets) {
        var payload = [];
        for (var i = 0; i < tweets.length; i++) {
            payload.push(tweets[i].text);
        }
        console.log('payload: ' + payload);
        return twitterService.analyzeWithIndico(payload);
    }

    $scope.getBestTweets = function(scores) {
        var localScores = angular.fromJson(scores)['results'];
        console.log('scores: ' + localScores);
        topScoreIndexes = $scope.findTopIndexes(localScores);
        console.log(topScoreIndexes);
        $scope.bestTweets = [
            $scope.tweets[topScoreIndexes[0]],
            $scope.tweets[topScoreIndexes[1]],
            $scope.tweets[topScoreIndexes[2]]
        ];
    }

    $scope.findTopIndexes = function(scores) {
        var topScores = [];
        var topIndexes = [];
        //this should be refactored
        for (var i = 0; i < scores.length; i++) {
            var str = i.toString();
            if (topScores.length < 3) {
                topScores.push(scores[i]);
            }
            if (topScores[0] < scores[i]) {
                topScores[0] = scores[i];
                topIndexes[0] = i;
            } else if (topScores[1] < scores[i]) {
                topScores[1] = scores[i];
                topIndexes[1] = i;
            } else if (topScores[2] < scores[i]) {
                topScores[2] = scores[i];
                topIndexes[2] = i;
            }
        }
        console.log(topIndexes);
        return topIndexes;

    }

    //when the user clicks the connect twitter button, the popup authorization window opens
    $scope.connectButton = function() {
        twitterService.connectTwitter().then(function() {
            if (twitterService.isReady()) {
                //if the authorization is successful, hide the connect button and display the tweets
                $('#connectButton').fadeOut(function() {
                    $('#signOut, #searchField').fadeIn();
                    $scope.connectedTwitter = true;
                });
            } else {

            }
        });
    }

    //sign out clears the OAuth cache, the user will have to reauthenticate when returning
    $scope.signOut = function() {
        twitterService.clearCache();
        $scope.tweets.length = 0;
        $('#signOut, #searchField').fadeOut(function() {
            $('#connectButton').fadeIn();
            $scope.$apply(function() {
                $scope.connectedTwitter = false
            })
        });
    }

    //if the user is a returning user, hide the sign in button and display the tweets
    if (twitterService.isReady()) {
        $('#connectButton').hide();
        $('#signOut, #searchField').show();
        $scope.connectedTwitter = true;
    }
});